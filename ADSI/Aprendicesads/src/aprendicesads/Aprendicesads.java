/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aprendicesads;

import java.util.Scanner;

/**
 *
 * @author casa
 */
public class Aprendicesads {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         Scanner in = new Scanner(System.in);
        int f,c,eva=1,tarea=1, opc;
        aprendiz s = new aprendiz();
        ficha s2 = new ficha();
        s.nombres();
        s2.numeroficha();
        s2.centrof();
        do{

            System.out.println("--------------Menu--------------");
            System.out.println("    1. Ingresar notas Java");
            System.out.println("    2. Ingresar notas PHP");
            System.out.println("    3. Ingresar notas MySql");
            System.out.println("    4. Ingresar notas Mongo");
            System.out.println("    5. Consutlar todas las notas");
            System.out.println("    6. Consultar el mejor promedio por materia");
            System.out.println("    7. Consultar el peor promedio por materia");
            System.out.println("    8. Consultar la materia mas facil");
            System.out.println("    9. Consultar la materia mas dificil");
            System.out.println("    10. Consultar el mejor estudiante del adsi");
            System.out.println("    0. Salir");
            System.out.println("--------------------------------");
            System.out.println("    Elija una opcion: ");
            opc = in.nextInt();
            switch(opc){
                case 1:
                    System.out.println("Ingrese la cantidad de pruebas  JAVA");
                    eva = in.nextInt();
                    System.out.println("Ingrese la cantidad de tareas  JAVA");
                    tarea=in.nextInt();
                    s.notasJava(eva, tarea);
                    break;
                case 2:
                    System.out.println("Ingrese la cantidad de pruebas  PHP");
                    eva = in.nextInt();
                    System.out.println("Ingrese la cantidad de tareas PHP");
                    tarea=in.nextInt();
                    s.notasPHP(eva, tarea);
                    break;
                case 3:
                    System.out.println("Ingrese la cantidad de pruebas MySql");
                    eva = in.nextInt();
                    System.out.println("Ingrese la cantidad de tareas  MySql");
                    tarea=in.nextInt();
                    s.notasMySql(eva, tarea);
                    break;
                case 4:
                    System.out.println("Ingrese la cantidad de pruebas  Mongo");
                    eva = in.nextInt();
                    System.out.println("Ingrese la cantidad de tareas  Mongo");
                    tarea=in.nextInt();
                    s.notasMongo(eva, tarea);
                    break;
                case 5:
                    s.general();
                    break;
                case 6:
                    s.mejorpromedio();
                    break;
                case 7:
                    s.peorpromedio();
                    break;
                case 8:
                    s.materiafacil();
                    break;
                case 9:
                    s.materiadificil();
                    break;
                case 10:
                    s.mejorest();
                    break;
            }
        }while(opc!=0);
        
    }
    
}
