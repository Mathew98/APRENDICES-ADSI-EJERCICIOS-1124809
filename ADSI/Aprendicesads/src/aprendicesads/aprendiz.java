/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aprendicesads;

import java.util.Scanner;

/**
 *
 * @author casa
 */
public class aprendiz extends ficha {
    
    Scanner on = new Scanner(System.in);
    int f=0,c=0,i=0, g = 1;
    double notas [][] = new double [29][21];
    String nombres [] = new String[29];
    public void nombres(){

        for(f=0; f<29; f++)
        {
                System.out.println("Ingrese nombre del estudiante  "+g); 

                nombres[f]= on.next();
                g++;
        }

        System.out.println("------------NOMBRE DE LOS ESTUDIANTES----------------------");
        g=1;
        for(f=0; f<29; f++)
        {
                System.out.println(g+"-"+nombres[f]+"\t");
                g++;
        }
        
    }

    
     public void notasJava(int cQuiz, int cTareas){
        double quiz, totalQuiz=0, tarea, totalTarea=0, totp3;
        double parcial3; 

        for(f=0; f<29; f++)
        { 
            for(c=0;c<4;c++)
            {
                if(c==2)
                {
                    notas[f][2]=0;
                }
                else
                {
                System.out.println("Ingrese la nota del parcial " + (c+1) + " de la materia JAVA del estudiante " + nombres[f]);
                notas[f][c] = on.nextDouble();
                }
            }
        }

        for (f=0; f<29; f++)
        {
            for (c=2; c<3; c++)
            {
               for(i=0; i<cQuiz; i++)
                {
                 
                    System.out.println("Por favor ingrese la nota del quiz " + (i+1) + " de la materia JAVA del estudiante: " + nombres[f]);
                
                    quiz = on.nextInt();
                
                    totalQuiz += quiz;
                }
           
                for(i=0; i<cTareas; i++)
                {
           
                System.out.println("Por favor ingrese la nota de la tarea " + (i+1) + " de la materia JAVA del estudiante: " + nombres[f]);

                tarea = on.nextInt();

                totalTarea += tarea;
            }

            totp3 = cTareas+cQuiz;
            parcial3 = totalQuiz+totalTarea;
            parcial3= parcial3/totp3;

            System.out.println("El promedio del parcial 3 de la materia JAVA es: " + parcial3);
            totalTarea=0;
            totalQuiz=0;
            totp3=0;
            quiz=0;
            tarea=0;

            notas[f][2] = parcial3;
            parcial3=0;
        }
    }

        for(f=0; f<29; f++)
        {

            notas[f][0] = notas[f][0] * 0.25;
            notas[f][1] = notas[f][1] * 0.25;
            notas[f][2] = notas[f][2] * 0.20;
            notas[f][3] = notas[f][3] * 0.30;
        }

        for(f=0;f<29;f++)
        {
  
            notas[f][4] = notas[f][0] + notas[f][1] + notas[f][2] + notas[f][3];
        }

        System.out.println("      |                                 JAVA                          |");
        System.out.println("Nombre| Parcial 1 | Parcial 2 |Parcial 3 | Parcial 4 | Promedio Final |");
        for(f=0;f<29; f++)
        {

            System.out.print(nombres[f]+ "\t\t");
            for(c=0; c<5; c++){
            System.out.print(notas[f][c]+"\t\t"); 
        }
            System.out.print("\n");
        }        
    }
    
    
    public void notasPHP(int cQuiz, int cTareas){
        double quiz, totalQuiz=0, tarea, totalTarea=0, totp3;
        double parcial3; 
        for(f=0; f<29; f++)
        { 
            for(c=5;c<9;c++)
            {
                if(c==7)
                {
                    notas[f][7]=0;
                }
                else
                {
                System.out.println("Ingrese la nota del parcial " + (c-4) + " de la materia PHP del estudiante " + nombres[f]);
                notas[f][c] = on.nextDouble();
                }
            }
        }

        for (f=0; f<29; f++)
        {
            for (c=7; c<8; c++)
            {
               for(i=0; i<cQuiz; i++)
                {
                  
                    System.out.println("Por favor ingrese la nota del quiz " + (i+1) + " de la materia PHP del estudiante: " + nombres[f]);
                  
                    quiz = on.nextInt();
                  
                    totalQuiz += quiz;
                }
         
                for(i=0; i<cTareas; i++)
                {
     
                System.out.println("Por favor ingrese la nota de la tarea " + (i+1) + " de la materia PHP del estudiante: " + nombres[f]);
             
                tarea = on.nextInt();
          
                totalTarea += tarea;
            }
         
            totp3 = cTareas+cQuiz;
            parcial3 = totalQuiz+totalTarea;
            parcial3= parcial3/totp3;
          
            System.out.println("El promedio del parcial 3 de la materia PHP es: " + parcial3);
            totalTarea=0;
            totalQuiz=0;
            totp3=0;
            quiz=0;
            tarea=0;
          
            notas[f][7] = parcial3;
            parcial3=0;
        }
    }
        
        for(f=0; f<29; f++)
        {
            notas[f][5] = notas[f][5] * 0.25;
            notas[f][6] = notas[f][6] * 0.25;
            notas[f][7] = notas[f][7] * 0.20;
            notas[f][8] = notas[f][8] * 0.30;
        }
 
        for(f=0;f<29;f++)
        {
            notas[f][9] = notas[f][5] + notas[f][6] + notas[f][7] + notas[f][8];
        }
     
        
        System.out.println("      |                         PHP                                   |");
        System.out.println("Nombre| Parcial 1 | Parcial 2 |Parcial 3 | Parcial 4 | Promedio Final |");
        for(f=0;f<29; f++)
        {
            System.out.print(nombres[f]+ "\t");
            for(c=5; c<10; c++){
            System.out.print(notas[f][c]+"\t"); 
        }
            System.out.print("\n");
        }        
    }
    

    public void notasMySql(int cQuiz, int cTareas){

        double quiz, totalQuiz=0, tarea, totalTarea=0, totp3;
 
        double parcial3; 
   
        for(f=0; f<29; f++)
        { 
            for(c=10;c<14;c++)
            {
                if(c==12)
                {
                    notas[f][12]=0;
                }
                else
                {
                 
                System.out.println("Ingrese la nota del parcial " + (c-9) + " de la materia MySql del estudiante " + nombres[f]);
                notas[f][c] = on.nextDouble();
                }
            }
        }

        for (f=0; f<29; f++)
        {
            for (c=12; c<13; c++)
            {
               for(i=0; i<cQuiz; i++)
                {
    
                    System.out.println("Por favor ingrese la nota del quiz " + (i+1) + " de la materia MySql del estudiante: " + nombres[f]);
            
                    quiz = on.nextInt();
           
                    totalQuiz += quiz;
                }
             
                for(i=0; i<cTareas; i++)
                {
              
                System.out.println("Por favor ingrese la nota de la tarea " + (i+1) + " de la materia MySql del estudiante: " + nombres[f]);
          
                tarea = on.nextInt();
        
                totalTarea += tarea;
            }
          
            totp3 = cTareas+cQuiz;
            parcial3 = totalQuiz+totalTarea;
            parcial3= parcial3/totp3;

            System.out.println("El promedio del parcial 3 de la materia MySql es: " + parcial3);
            totalTarea=0;
            totalQuiz=0;
            totp3=0;
            quiz=0;
            tarea=0;

            notas[f][12] = parcial3;
            parcial3=0;
        }
    }
      
        for(f=0; f<29; f++)
        {
          
            notas[f][10] = notas[f][10] * 0.25;
            notas[f][11] = notas[f][11] * 0.25;
            notas[f][12] = notas[f][12] * 0.20;
            notas[f][13] = notas[f][13] * 0.30;
        }
       
        for(f=0;f<29;f++)
        {
           
            notas[f][14] = notas[f][10] + notas[f][11] + notas[f][12] + notas[f][13];
        }
       
       
        System.out.println("      |                                 MySqL                         |");
        System.out.println("Nombre| Parcial 1 | Parcial 2 |Parcial 3 | Parcial 4 | Promedio Final |");
        for(f=0;f<29; f++)
        {
           
            System.out.print(nombres[f]+ "\t \t");
            for(c=10; c<15; c++){
            System.out.print(notas[f][c]+"\t \t"); 
        }
            System.out.print("\n");
        }        
    }

    
        public void notasMongo(int cQuiz, int cTareas){
        
        double quiz, totalQuiz=0, tarea, totalTarea=0, totp3;
        double parcial3; 
       
        for(f=0; f<29; f++)
        { 
            for(c=15;c<19;c++)
            {
                if(c==17)
                {
                   
                    notas[f][17]=0;
                }
                else
                {
               
                System.out.println("Ingrese la nota del parcial " + (c-14) + " de la materia Mongo del estudiante " + nombres[f]);
                notas[f][c] = on.nextDouble();
                }
            }
        }

        for (f=0; f<29; f++)
        {
            for (c=17; c<18; c++)
            {
               for(i=0; i<cQuiz; i++)
                {
            
                    System.out.println("Por favor ingrese la nota del quiz " + (i+1) + " de la materia Mongo del estudiante: " + nombres[f]);
                   
                    quiz = on.nextInt();
            
                    totalQuiz += quiz;
                }
               
                for(i=0; i<cTareas; i++)
                {
              
                System.out.println("Por favor ingrese la nota de la tarea " + (i+1) + " de la materia Mongo del estudiante: " + nombres[f]);
              
                tarea = on.nextInt();
          
                totalTarea += tarea;
            }
         
            totp3 = cTareas+cQuiz;
            parcial3 = totalQuiz+totalTarea;
            parcial3= parcial3/totp3;
         
            System.out.println("El promedio del parcial 3 de la materia Mongo es: " + parcial3);
            totalTarea=0;
            totalQuiz=0;
            totp3=0;
            quiz=0;
            tarea=0;
     
            notas[f][17] = parcial3;
            parcial3=0;
        }
    }
       
        for(f=0; f<29; f++)
        {
          
            notas[f][15] = notas[f][15] * 0.25;
            notas[f][16] = notas[f][16] * 0.25;
            notas[f][17] = notas[f][17] * 0.20;
            notas[f][18] = notas[f][18] * 0.30;
        }
       
        for(f=0;f<29;f++)
        {
           
            notas[f][19] = notas[f][15] + notas[f][16] + notas[f][17] + notas[f][18];
        }
       
        System.out.println("      |                                 Mongo                         |");
        System.out.println("Nombre| Parcial 1 | Parcial 2 |Parcial 3 | Parcial 4 | Promedio Final |");
        for(f=0;f<29; f++)
        {
           
            System.out.print(nombres[f]+ "\t \t");
            for(c=15; c<20; c++){
            System.out.print(notas[f][c]+"\t \t"); 
        }
            System.out.print("\n");
        }  
        }
        
        
        public void general(){
            System.out.println("  |                        JAVA                                   |                        PHP                                   |                        MYSQL                                  |                               Mongo                           |");
        System.out.println("Nombre| Parcial 1 | Parcial 2 |Parcial 3 | Parcial 4 | Promedio Final |Parcial 1 | Parcial 2 |Parcial 3 | Parcial 4 | Promedio Final | Parcial 1 | Parcial 2 |Parcial 3 | Parcial 4 | Promedio Final | Parcial 1 | Parcial 2 |Parcial 3 | Parcial 4 | Promedio Final |");
            for(f=0;f<29; f++)
        {
            System.out.print(nombres[f]+ "\t ");
            for(c=0; c<20; c++){
            System.out.print(notas[f][c]+"\t"); 
        }
            System.out.print("\n");
        } 
        }
        
        public void mejorpromedio(){
             
            double promedioj=0,  promediop=0, promediom=0, promediomo=0;
            String estudiante="";
           
            for(f=0; f<29; f++)
            {
                for(c=4; c<5; c++)
                {
                  
                    if(promedioj<notas[f][c])
                    {
                    promedioj = notas[f][c];
                    estudiante = nombres[f];
                    }
                }
            }
            System.out.println("El mejor promedio de Java es: " + promedioj + " del estudiante: " + estudiante);
         
            for(f=0; f<29; f++)
            {
                for(c=9; c<10; c++)
                {
                    if(promediop<notas[f][c])
                    {
                    promediop = notas[f][c];
                    estudiante = nombres[f];
                    }
                }
            }
            System.out.println("El mejor promedio de PHP es: " + promediop + " del estudiante: " + estudiante);
        
            for(f=0; f<29; f++)
            {
                for(c=14; c<15; c++)
                {
                    if(promediom<notas[f][c])
                    {
                    promediom = notas[f][c];
                    estudiante = nombres[f];
                    }
                }
            }
          
            System.out.println("El mejor promedio de MySql es: " + promediom + " del estudiante: " + estudiante);
         
            for(f=0; f<29; f++)
            {
                for(c=19; c<20; c++)
                {
                    if(promediomo<notas[f][c])
                    {
                    promediomo = notas[f][c];
                    estudiante = nombres[f];
                    }
                }
            }
            System.out.println("El mejor promedio de Mongo es: " + promediomo + " del estudiante: " + estudiante);
        }
        
        
        public void peorpromedio(){
      
            double  promedioj=6,  promediop=6, promediom=6, promediomo=6;
            String estudiante="";
        
            for(f=0; f<29; f++)
            {
                for(c=4; c<5; c++)
                {
               
                    if(promedioj>notas[f][c])
                    {
                    promedioj = notas[f][c];
                    estudiante = nombres[f];
                    }
                }
            }
          
            System.out.println("El peor promedio de Java es: " + promedioj + " del estudiante: " + estudiante);
      
            for(f=0; f<29; f++)
            {
                for(c=9; c<10; c++)
                {
                    if(promediop>notas[f][c])
                    {
                    promediop = notas[f][c];
                    estudiante = nombres[f];
                    }
                }
            }
            System.out.println("El peor promedio de PHP es: " + promediop + " del estudiante: " + estudiante);
       
            for(f=0; f<29; f++)
            {
                for(c=14; c<15; c++)
                {
                    if(promediom>notas[f][c])
                    {
                    promediom = notas[f][c];
                    estudiante = nombres[f];
                    }
                }
            }
            System.out.println("El peor promedio de MySql es: " + promediom + " del estudiante: " + estudiante);
     
            for(f=0; f<29; f++)
            {
                for(c=19; c<20; c++)
                {
                    if(promediomo>notas[f][c])
                    {
                    promediomo = notas[f][c];
                    estudiante = nombres[f];
                    }
                }
            }
            System.out.println("El peor promedio de Mongo es: " + promediomo + " del estudiante: " + estudiante);
        }
        
        public void materiafacil(){
      
            double valorj=0, valorp=0, valorm=0, valormo=0;
            for(f=0; f<29; f++)
            {
                for(c=4; c<5; c++)
                {
             
                    valorj += notas[f][c];
                }
            }
     
            valorj /= 3;
            for(f=0; f<29; f++)
            {
                for(c=9; c<10; c++)
                {
                   
                    valorp += notas[f][c];
                }
            }
        
            valorp /= 3;
            for(f=0; f<29; f++)
            {
                for(c=14; c<15; c++)
                {
                  
                    valorm += notas[f][c];
                }
            }
         
            valorm /= 3;
            for(f=0; f<29; f++)
            {
                for(c=9; c<10; c++)
                {
                   
                    valormo += notas[f][c];
                }
            }
         
            valormo /= 3;

            if(valorj>valorp && valorj>valorm && valorj>valormo)
            {
                
                System.out.println("La materia más fácil es: JAVA");
            }
            else if (valorj<valorp && valorp>valorm && valorp>valormo)
            {
              
                System.out.println("La materia más fácil es: PHP");
            }
            else if (valorm>valorj && valorm>valorp && valorm>valormo)
            {
               
                System.out.println("La materia más fácil es: MySql");
            }
            else if (valormo>valorj && valormo>valorp && valorm<valormo)
            {
               
                System.out.println("La materia más fácil es: Mongo");
            }
        }
        
        
        public void materiadificil(){
         
            double valorj=0, valorp=0, valorm=0, valormo=0;
            for(f=0; f<29; f++)
            {
                for(c=4; c<5; c++)
                {
                   
                    valorj += notas[f][c];
                }
            }
          
            valorj /= 3;
            for(f=0; f<29; f++)
            {
                for(c=9; c<10; c++)
                {
                    
                    valorp += notas[f][c];
                }
            }
           
            valorp /= 3;
            for(f=0; f<29; f++)
            {
                for(c=14; c<15; c++)
                {
                   
                    valorm += notas[f][c];
                }
            }
           
            valorm /= 3;
            for(f=0; f<29; f++)
            {
                for(c=19; c<20; c++)
                {
                   
                    valormo += notas[f][c];
                }
            }
           
            valormo/=3;
            double total;
            String materia="";
            total = valorj;
            materia="Java";
       
            if(total>valorp)
            {
                total = valorp;
                materia="PHP";
            }
            if(total>valorm){
                 total = valorm;
                 materia = "MySql";
            }
            if(total>valormo){
                total=valormo;
                materia="Mongo";
            }
       
            System.out.println("La materia mas dificil es: " +materia);
        }
        
        public void mejorest(){
          
            String estudiante ="";
          
            double promediomayor=0;
            for(f=0; f<29; f++)
            {
                for(c=20; c<21; c++)
                {
                  
                    notas[f][c]= notas[f][4] + notas[f][9] + notas[f][14] + notas[f][19];
                }
            }
            for(f=0; f<29; f++)
            {
            if (promediomayor<notas[f][20])
                {
                   
                    promediomayor = notas[f][20];       
                    estudiante = nombres[f];
                }
            }
           
            System.out.println("El mejor estudiante de adsi es: " + estudiante + " con un promedio total de: " + promediomayor);
            
        }
}




