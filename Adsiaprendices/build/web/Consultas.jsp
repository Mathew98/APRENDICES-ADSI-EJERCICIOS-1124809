<%-- 
    Document   : Consultas
    Created on : 12-abr-2016, 20:30:24
    Author     : casa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/Style.css">
        <title>APRENDICES SENA</title>
    </head>
    <body>
        <section id="aprend">  
                <h1>APRENDICES SENA</h1>
        </section>
        <%
            double promjava = 0;
            double promphp = 0;
            double prommy = 0;
            double prommo = 0;
            double h = 0;
            double k = 4;
            double f = 0, n = 0;           
            int y = 0 , m = 0  ;
            
            int q = Integer.parseInt(request.getParameter("ki"));
            String nombreW[] = new String[29];
            double promedios[] = new double[29];
            double promediosj[] = new double[29];
            double promediosp[] = new double[29];
            double promediosm[] = new double[29];
            double promediosmo[] = new double[29];
            
                    for(int i=0;i<q;i++){
                        nombreW[i] = request.getParameter("vb"+i);
                        }                    
        %>
    <section id="table">
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <td>NOMBRES</td>
                     <td>PROMEDIO JAVA</td>
                     <td>PROMEDIO PHP</td>
                     <td>PROMEDIO MySQL</td>
                     <td>PROMEDIO MONGO</td>
                     <td>PROMEDIO MATERIAS</td>
                </tr>
                
                    <%
                    
                for(int i=0;i<q;i++){
                        double t = 0, b = 0, a = 0, x = 0;
                     %>
                <tr>


                      <td><%out.println(nombreW[i]);%></td>
                      
                      <td><%

                            for(int u=y;u<k;u++){
                             h = Double.parseDouble(request.getParameter("ja"+u));
                                 promjava = h +promjava;
                                 
                                }
                            
                            out.println(promjava/4);
                            t = promjava/4;
                            promediosj[i] = t;
                            promjava = 0; 
                            %>
                      </td>
                      <td><%
                            for(int u=y;u<k;u++){
                             h = Double.parseDouble(request.getParameter("ph"+u));
                                promphp = h +promphp;
                            }
                            
                            
                            out.println(promphp/4);
                            b = promphp/4;
                            promediosp[i] = b;
                            promphp = 0;
                            %>
                      </td>
                      <td><%
                            for(int u=y;u<k;u++){
                                h = Double.parseDouble(request.getParameter("my"+u));
                                prommy = h +prommy;
                            }
                            
                            out.println(prommy/4);
                            a = prommy/4;
                            promediosm[i] = a;
                            prommy = 0;
                            %>
                                
                      </td>
                      <td><%
                           for(int u=y;u<k;u++){
                                h = Double.parseDouble(request.getParameter("mo"+u));
                                    prommo = h +prommo;
                            }
                            
                            out.println(prommo/4);
                            x = prommo/4;
                            promediosmo[i] = x;
                            prommo = 0;
                          %>
                        
                      </td>


                      <td><%
                            out.println((t+b+a+x)/4);
                            f = (t+b+a+x)/4;
                            promedios[m] = f;
                            m++;
                          %>
                      </td>         
                </tr>
                     <%

                          k = k + 4;
                          y = y + 4;
                        }
                     %>

            </table>
        </div>
    </section>
        <section id="mejored">
            <h4>
            <%
            String nom = "";
            for(int v = 0;v<q;v++){

                 if(n<promedios[v]){
                        n = promedios[v];
                        nom = nombreW[v];
                    }
                }
            out.println("El mejor estudiante ADSI es: "+nom+", con un promedio materias de: "+n);
            %>
           </h4>
        </section>
        <section id="aprend">  
                <h2>JAVA</h2>
        </section>
        <section id="j">
            <h4>
            <%
            for(int v = 0;v<q;v++){

                 if(n<promediosj[v]){
                        n = promediosj[v];
                        nom = nombreW[v];
                    }
                }
            out.println("El mejor estudiante es: "+nom+", con un promedio de: "+n);
            %>
                <br>
                <br>
            <%
            for(int v = 0;v<q;v++){

                 if(n>promediosj[v]){
                        n = promediosj[v];
                        nom = nombreW[v];
                    }
                }
            out.println("El peor estudiante es: "+nom+", con un promedio de: "+n);
            %>
            </h4>
        </section>
        <section id="aprend">  
                <h2>PHP</h2>
        </section>
        <section id="p">
            <h4>
            <%
            for(int v = 0;v<q;v++){

                 if(n<promediosp[v]){
                        n = promediosp[v];
                        nom = nombreW[v];
                    }
                }
            out.println("El mejor estudiante  es: "+nom+", con un promedio de: "+n);
            %>
                <br>
                <br>
            <%
            for(int v = 0;v<q;v++){

                 if(n>promediosp[v]){
                        n = promediosp[v];
                        nom = nombreW[v];
                    }
                }
            out.println("El peor estudiante  es: "+nom+", con un promedio de: "+n);
            %>
            </h4>
        </section>
        <section id="aprend">  
                <h2>MONGOL</h2>
        </section>
        <section id="mo">
            <h4>
            <%
            for(int v = 0;v<q;v++){

                 if(n<promediosmo[v]){
                        n = promediosmo[v];
                        nom = nombreW[v];
                    }
                }
            out.println("El mejor estudiante  es: "+nom+", con un promedio de: "+n);
            %>
                <br>
                <br>
            <%
            for(int v = 0;v<q;v++){

                 if(n>promediosmo[v]){
                        n = promediosmo[v];
                        nom = nombreW[v];
                    }
                }
            out.println("El peor estudiante  es: "+nom+", con un promedio de: "+n);
            %>
            </h4>
        </section>
        <section id="aprend">  
                <h2>MySQL</h2>
        </section>
        <section id="my">
            <h4>
            <%
            for(int v = 0;v<q;v++){

                 if(n<promediosm[v]){
                        n = promediosm[v];
                        nom = nombreW[v];
                    }
                }
            out.println("El mejor estudiante  es: "+nom+", con un promedio de: "+n);
            %>
                <br>
                <br>
            <%
            for(int v = 0;v<q;v++){

                 if(n>promediosm[v]){
                        n = promediosm[v];
                        nom = nombreW[v];
                    }
                }
            out.println("El peor estudiante  es: "+nom+", con un promedio de: "+n);
            %>
            </h4>
        </section>
        <section id="mejored">
            <h4>
            <%
                double valorj=0, valorp=0, valorm=0, valormo=0;
                for(int v = 0;v<q;v++)
                {
                        valorj = valorj + promediosj[v];
                }
                for(int v = 0;v<q;v++)
                {
                        valorp = valorp + promediosp[v];
                }

                for(int v = 0;v<q;v++)
                {
                        valorm = valorm + promediosm[v];
                }

                for(int v = 0;v<q;v++)
                {
                        valormo = valormo + promediosmo[v];
                }
                if(valorj>valorp&&valorj>valorm&&valorj>valormo)
                {
                    out.println("La materia más fácil es: JAVA");
                }
                else if (valorj<valorp&&valorp>valorm&&valorp>valormo)
                {

                    out.println("La materia más fácil es: PHP");
                }
                else if (valorm>valorj&&valorm>valorp&&valorm>valormo)
                {

                    out.println("La materia más fácil es: MySql");
                }
                else if (valormo>valorj&&valormo>valorp&&valorm<valormo)
                {

                    out.println("La materia más fácil es: Mongo");
                }
               %>
               </h4>
         </section>
        <a href="index.jsp" onclick="salir()" id="as1" class="btn btn-primary btn-lg btn-block">SALIR</a><br>
    </body>
</html>
